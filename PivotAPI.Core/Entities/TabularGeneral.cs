﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PivotAPI.Core.Entities
{
    public class TabularGeneral
    {
        public decimal AnioAnt { get; set; }
        public int AnioAntPorc { get; set; }
        public decimal SemAnt { get; set; }
        public int SemAntPorc { get; set; }
        public decimal Objetivo { get; set; }
        public int ObjetivoPorc { get; set; }
        public decimal Real { get; set; }
        public int RealPorc { get; set; }
        public int idTabular { get; set; }
        public string Descripcion { get; set; }
        public decimal VsObjetivo { get; set; }
        public int VsObjetivoPorc { get; set; }
        public decimal VsSemAnt { get; set; }
        public int VsSemAntPorc { get; set; }
        public decimal VsAnioAnt { get; set; }
        public int VsAnioAntPorc { get; set; }


    }
}
