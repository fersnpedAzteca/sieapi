﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PivotAPI.Core.Entities
{

    public class Unidades
    {
        public string unidades { get; set; }
        public string miles
        {
            get { return (Convert.ToDouble(unidades) / 1000).ToString(); }
        }
        public string millones
        {
            get { return (Convert.ToDouble(unidades) / 1000000).ToString(); }
        }
        public string porcentaje { get; set; }

    }
}