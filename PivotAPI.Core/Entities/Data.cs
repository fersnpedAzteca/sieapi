﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PivotAPI.Core.Entities
{
    public class Data
    {


        public List<Descripcion> descripcion { get; set; }
        public List<Unidades> anio_anterior { get; set; }
        public List<Unidades> semana_anterior { get; set; }
        public List<Unidades> objetivo { get; set; }
        public List<Unidades> mtoreal { get; set; }
        public List<Unidades> vs_objetivo { get; set; }
        public List<Unidades> vs_semana_anterior { get; set; }
        public List<Unidades> vs_anio_anterior { get; set; }

    }
}
