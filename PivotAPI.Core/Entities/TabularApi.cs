﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PivotAPI.Core.Entities
{
    public class TabularApi
    {
        public List<Columna> columna { get; set; }
        public List<Data> data { get; set; }
    }
}
