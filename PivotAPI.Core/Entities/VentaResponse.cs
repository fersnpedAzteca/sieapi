﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PivotAPI.Core.Entities
{
    public class VentaResponse
    {
        public string ventaActual { get; set; }
        public string ventaPresupuesto { get; set; }

        public string ventaAnoAnterior { get; set; }


    }
}
