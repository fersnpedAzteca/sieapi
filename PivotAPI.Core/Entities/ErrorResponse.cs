﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;
using PivotAPI.Core.Entities;

namespace PivotAPI.Core.Entities
{
    public class ErrorResponse
    {
        public string codigo;
        public string mensaje;
        public string folio;
        public string info;
        public ArrayList detalles;

        //public ErrorResponse(string codigo, string mensaje, string folio, string info, ArrayList detalles)
        //{
        //    this.codigo = codigo;
        //    this.mensaje = mensaje;
        //    this.folio = folio;
        //    this.info = info;
        //    this.detalles = detalles;

        //}
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
