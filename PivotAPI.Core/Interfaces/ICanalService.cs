﻿using PivotAPI.Core.Entities;
using PivotAPI.Core.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace PivotAPI.Core.Interfaces
{
    public interface ICanalService
    {
        Task<List<TabularApi>> GetCanal(ConsultaTabular tabularQuery);
    }
}