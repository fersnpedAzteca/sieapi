﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using PivotAPI.Core.Entities;

namespace PivotAPI.Core.Interfaces
{
    public interface ITabularConversion
    {
        List<TabularGeneral> GenerarPorcentaje(DataTable data);
        List<TabularApi> GenerarDetalle(DataSet data);
        DataTable GenerarTabularDeDataSet(DataSet data);

    }
}
