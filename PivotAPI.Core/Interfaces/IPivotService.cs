﻿using System.Data;
using System.Threading.Tasks;

namespace PivotAPI.Core.Interfaces
{
    public interface IPivotService
    {
        Task<DataTable> GetPivot(string inf, string sup);
    }
}