﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using PivotAPI.Core.Entities;

namespace PivotAPI.Core.Interfaces
{
    public interface IGraficaConversion
    {
        DataTable Pivot(DataTable dt, DataColumn pivotColumn, DataColumn pivotValue);
        List<TabularApi> GenerarDetalle(DataSet data);
    }
}
