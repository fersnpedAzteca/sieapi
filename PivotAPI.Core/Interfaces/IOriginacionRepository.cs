﻿using PivotAPI.Core.Filters;
using System;
using System.Data;
using System.Threading.Tasks;

namespace PivotAPI.Core.Interfaces
{
    public interface IOriginacionRepository
    {
        Task<DataTable> GetOriginacion(ConsultaTabular tabularQuery);
    }
}
