﻿using PivotAPI.Core.Filters;
using System;
using System.Data;
using System.Threading.Tasks;

namespace PivotAPI.Core.Interfaces
{
    public interface IGraficaColocacionRepository
    {
        Task<DataTable> GetGraficaColocacion(ConsultaTabular tabularQuery);
    }
}