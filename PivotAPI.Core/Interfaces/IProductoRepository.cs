﻿using PivotAPI.Core.Filters;
using System;
using System.Data;
using System.Threading.Tasks;

namespace PivotAPI.Core.Interfaces
{
    public interface IProductoRepository
    {
        Task<DataTable> GetProducto(ConsultaTabular tabularQuery);
    }
}