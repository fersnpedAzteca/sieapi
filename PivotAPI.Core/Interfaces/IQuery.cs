﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PivotAPI.Core.Interfaces
{
    public interface IQuery
    {
        string AddParams(string query, string parameterName, string parameterValue);
        string GetValue(string prop);
    }
}
