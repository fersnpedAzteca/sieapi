﻿using PivotAPI.Core.Filters;
using System;
using System.Data;
using System.Threading.Tasks;

namespace PivotAPI.Core.Interfaces
{
    public interface IFormatoRepository
    {
        Task<DataTable> GetFormato(ConsultaTabular tabularQuery);
    }
}