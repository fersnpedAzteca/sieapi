﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PivotAPI.Core.Filters
{
    public class ConsultaTabular
    {
        public string fecha { get; set; }
        public int? periodo { get; set; }
        public int? tipoCliente { get; set; }
        public int? participacion { get; set; }
        public int? dia { get; set; }
        public int? geografia { get; set; }
        public int? producto { get; set; }
        public int? originacion { get; set; }
        public int? formato { get; set; }

    }
}
