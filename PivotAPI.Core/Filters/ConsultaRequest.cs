﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PivotAPI.Core.Filters
{
    public class ConsultaRequest
    {
        public string fecha { get; set; }
        public string? periodo { get; set; }
        public string? tipoCliente { get; set; }
        public string? participacion { get; set; }
        public string? dia { get; set; }
        public string? geografia { get; set; }
        public string? producto { get; set; }
        public string? originacion { get; set; }
        public string? formato { get; set; }

    }
}
