﻿using LoggerService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using System.Net;
using PivotAPI.Core.Entities;

namespace ColocacionAPI.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExeptionHandler(this IApplicationBuilder app, ILoggerManager logger)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        logger.LogError($"Something went wrong: {contextFeature.Error}");
                        ArrayList arrayDetalles = new ArrayList();
                        arrayDetalles.Add(contextFeature.Error);
                        await context.Response.WriteAsync(new ErrorResponse()
                        {

                            codigo = "404.SIE-Colocacion-Indicadores.2000",
                            mensaje = "No se encontró información",
                            folio = getFolio(),
                            info = GetIP(),
                            detalles = arrayDetalles

                        }.ToString());
                    }
                });
            });
        }
        public static string GetIP()
        {
            string hostName = System.Net.Dns.GetHostName();
            string myIP = System.Net.Dns.GetHostEntry(hostName).AddressList[0].ToString();
            return myIP.Substring(myIP.Length - 3);
        }

        public static string getFolio()
        {
            string folio = "";
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            byte[] byteArray = new byte[4];
            provider.GetBytes(byteArray);
            UInt32 randomNumber = BitConverter.ToUInt32(byteArray, 0);
            string numberRamdom = string.Empty;
            if (randomNumber.ToString().Length > 7)
            {
                numberRamdom = randomNumber.ToString().Substring(0, 7);
            }
            else if (randomNumber.ToString().Length < 7)
            {
                numberRamdom = randomNumber.ToString("D8");
            }
            else
            {
                numberRamdom = randomNumber.ToString();
            }

            folio = DateTime.Now.ToString("yyyyMMddHHmmss") + numberRamdom;

            return folio;
        }
    }
}
