﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using PivotAPI.Core.Entities;

namespace SIEAPI.Responses
{

    public class ApiResponse<T>
    {
        public ApiResponse(T data)
        {
            Resultado = data;
        }
        public string Mensaje { get; set; }
        public string Folio { get; set; }
        public T Resultado { get; set; }
    }
    
}
