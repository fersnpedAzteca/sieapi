﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PivotAPI.Core.Filters;

namespace SIEAPI.Responses
{
    public class APIUtilities
    {
        public ConsultaTabular validarParametrosNulos(ConsultaRequest parametros)
        {
            ConsultaTabular consulta = new ConsultaTabular();
            DateTime fecha;

            if (parametros.fecha.Equals("null"))
            {
                throw new Exception("Internal server error.");
            }
            else
            {
                if (parametros.fecha.Count()== 8)
                {
                    fecha = Convert.ToDateTime(parametros.fecha.Substring(0, 4) + "-" + parametros.fecha.Substring(4, 2) + "-" + parametros.fecha.Substring(6, 2));
                    consulta.fecha = fecha.ToString("yyyy-MM-dd");
                }
                else
                    throw new Exception("Formato de fecha no es correcto.");
            }
            if (parametros.periodo == null)
            {
                //throw new Exception("Internal server error.");
            }
            else
            {
                if (parametros.periodo == "null")
                    consulta.periodo = null;
                else
                    consulta.periodo = Convert.ToInt32(parametros.periodo.ToString());
            }
            if (parametros.tipoCliente == null)
            {
                throw new Exception("Internal server error.");
            }
            else
            {
                if (parametros.tipoCliente == "null")
                    consulta.tipoCliente = null;
                else
                    consulta.tipoCliente = Convert.ToInt32(parametros.tipoCliente.ToString());
            }
            if (parametros.participacion == null)
            {
                //throw new Exception("Internal server error.");
            }
            else
            {
                if (parametros.participacion == "null")
                    consulta.participacion = null;
                else
                    consulta.participacion = Convert.ToInt32(parametros.participacion.ToString());
            }
            if (parametros.dia == null)
            {
                throw new Exception("Internal server error.");
            }
            else
            {
                if (parametros.dia == "null")
                    consulta.dia = null;
                else
                    consulta.dia = Convert.ToInt32(parametros.dia.ToString());
            }
            if (parametros.geografia == null)
            {
                //throw new Exception("Internal server error.");
            }
            else
            {
                if (parametros.geografia == "null")
                    consulta.geografia = null;
                else
                    consulta.geografia = Convert.ToInt32(parametros.geografia.ToString());
            }
            if (parametros.producto == null)
            {
                //throw new Exception("Internal server error.");
            }
            else
            {
                if (parametros.producto == "null")
                    consulta.producto = null;
                else
                    consulta.producto = Convert.ToInt32(parametros.producto.ToString());
            }
            if (parametros.originacion == null)
            {
                throw new Exception("Internal server error.");
            }
            else
            {
                if (parametros.originacion == "null")
                    consulta.originacion = null;
                else
                    consulta.originacion = Convert.ToInt32(parametros.originacion.ToString());
            }
            if (parametros.formato == null)
            {
                //throw new Exception("Internal server error.");
            }
            else
            {
                if (parametros.formato=="null")
                    consulta.formato = null;
                else
                    consulta.formato = Convert.ToInt32(parametros.formato.ToString());
            }
            return consulta;
        }
    }
}
