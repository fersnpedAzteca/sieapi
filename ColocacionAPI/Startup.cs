using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using PivotAPI.Core.Interfaces;
using PivotAPI.Core.Filters;
using PivotAPI.Core.Entities;
using PivotAPI.Infrastructure.Data;
using PivotAPI.Infrastructure.Repositories;
using PivotAPI.Infrastructure.Utils;
using PivotAPI.Services.Services;
using PivotAPI.Services.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using NLog;
using LoggerService;
using ColocacionAPI.Extensions;

namespace ColocacionAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            LogManager.LoadConfiguration(string.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, LoggerManager>();
            services.AddControllers().AddNewtonsoftJson(x =>
                x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddDbContext<VerticaContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("Vertica"),
               sqlServerOptions => sqlServerOptions.CommandTimeout(200)
               )
               );

            services.AddTransient<IGraficaBrutaRepository, GraficaBrutaRepository>();
            services.AddTransient<IGraficaBrutaService, GraficaBrutaService>();
            services.AddTransient<IGraficaColocacionRepository, GraficaColocacionRepository>();
            services.AddTransient<IGraficaColocacionService, GraficaColocacionService>();
            services.AddTransient<ICanalRepository, CanalRepository>();
            services.AddTransient<ICanalService, CanalService>();
            services.AddTransient<IDiarioRepository, DiarioRepository>();
            services.AddTransient<IDiarioService, DiarioService>();
            services.AddTransient<IOriginacionRepository, OriginacionRepository>();
            services.AddTransient<IOriginacionService, OriginacionService>();
            services.AddTransient<IGeografiaRepository, GeografiaRepository>();
            services.AddTransient<IGeografiaService, GeografiaService>();
            services.AddTransient<IFormatoRepository, FormatoRepository>();
            services.AddTransient<IFormatoService, FormatoService>();
            services.AddTransient<IProductoRepository, ProductoRepository>();
            services.AddTransient<IProductoService, ProductoService>();
            services.AddTransient<IQuery, Query>();
            services.AddTransient<IGraficaConversion, GraficaConversion>();


            services.AddTransient<ITabularConversion, TabularConversion>();


            services.AddSwaggerGen(doc =>
            {
                doc.SwaggerDoc("v1", new OpenApiInfo { Title = "Colocación API ", Version = "V1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                doc.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerManager logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.ConfigureExeptionHandler(logger);

            app.UseHttpsRedirection();

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("../swagger/v1/swagger.json", "Ventas API");
                options.RoutePrefix = "docs";
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
