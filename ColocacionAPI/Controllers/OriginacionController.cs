﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using Newtonsoft.Json;
using SIEAPI.Responses;
using PivotAPI.Core.Entities;
using System.Data;
using Newtonsoft.Json.Linq;
using PivotAPI.Core.Filters;
using System.Web.Http.Controllers;
using LoggerService;

namespace ColocacionAPI.Controllers
{
    [Route("sieapi/colocacion/indicador/[controller]")]
    [ApiController]
    public class OriginacionController : ControllerBase
    {
        private readonly IOriginacionService _originacionService;
        //private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;

        public OriginacionController(IOriginacionService originacionService, ILoggerManager logger)
        {
            _logger = logger;
            _originacionService = originacionService;
        }
        /// <summary>
        /// Devuelve consulta de tabular de colocacion de originación 
        /// </summary>
        /// <remarks>
        /// Se ingresa la fecha a consultar con los paramentros opcionales correspondientes
        /// </remarks>
        /// <param name="fecha"></param>
        /// <param name="periodo"></param>
        /// <param name="tipoCliente"></param>
        /// <param name="participacion"></param>
        /// <param name="dia"></param>
        /// <param name="geografia"></param>
        /// <param name="producto"></param>
        /// <param name="originacion"></param>
        /// <param name="formato"></param>
        /// 
        /// 
        /// 
        /// 
        /// 
        /// <returns></returns>
        /// <response code="200">Respuesta correcta</response>
        /// <response code="400">Fecha incorrecta o faltante</response>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponse<List<TabularGeneral>>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetOriginacion([FromBody] ConsultaRequest tabularQuery)
        {
            APIUtilities apiUtilities = new APIUtilities();
            ActionContext response = new ActionContext();
            try
            {
                ConsultaTabular tabularconsulta = new ConsultaTabular();
                _logger.LogInfo("Fetching all the Students from the storage");
                tabularconsulta = apiUtilities.validarParametrosNulos(tabularQuery);
                //if (!string.IsNullOrEmpty(error))
                //{
                //    return StatusCode(400, "Internal server error");
                //}
                var dataPivot = await _originacionService.GetOriginacion(tabularconsulta);

                string dataJson = JsonConvert.SerializeObject(dataPivot, Formatting.Indented);
                var res = new ApiResponse<List<TabularApi>>(dataPivot)
                {
                    Folio = "354303514351538131358",
                    Mensaje = "Operación Exitosa"
                };


                return Ok(res);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
