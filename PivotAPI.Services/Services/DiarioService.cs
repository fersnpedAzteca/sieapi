﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Core.Filters;
using PivotAPI.Core.Entities;

namespace PivotAPI.Services.Services
{
    public class DiarioService : IDiarioService
    {
        private readonly IDiarioRepository _diarioRepository;
        private readonly ITabularConversion _tabularconversion;
        public DiarioService(IDiarioRepository diarioRepository, ITabularConversion tabularconversion)
        {
            _diarioRepository = diarioRepository;
            _tabularconversion = tabularconversion;
        }
        public async Task<List<TabularApi>> GetDiario(ConsultaTabular tabularQuery)
        {
            DataSet resultado = new DataSet();
            DataTable result = await _diarioRepository.GetDiario(tabularQuery);
            resultado.Tables.Add(result);

            return _tabularconversion.GenerarDetalle(resultado);

            //return _tabularconversion.GenerarPorcentaje(result);

        }
    }
}
