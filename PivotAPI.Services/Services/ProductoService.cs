﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Core.Filters;
using PivotAPI.Core.Entities;

namespace PivotAPI.Services.Services
{
    public class ProductoService : IProductoService
    {
        private readonly IProductoRepository _productoRepository;
        private readonly ITabularConversion _tabularconversion;
        public ProductoService(IProductoRepository productoRepository, ITabularConversion tabularconversion)
        {
            _productoRepository = productoRepository;
            _tabularconversion = tabularconversion;
        }
        public async Task<List<TabularApi>> GetProducto(ConsultaTabular tabularQuery)
        {
            DataSet resultado = new DataSet();
            DataTable result = await _productoRepository.GetProducto(tabularQuery);
            resultado.Tables.Add(result);

            return _tabularconversion.GenerarDetalle(resultado);

            //return _tabularconversion.GenerarPorcentaje(result);
        }
    }
}
