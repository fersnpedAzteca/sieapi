﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Core.Filters;
using PivotAPI.Core.Entities;

namespace PivotAPI.Services.Services
{
    public class CanalService : ICanalService
    {
        private readonly ICanalRepository _canalRepository;
        private readonly ITabularConversion _tabularconversion;
        public CanalService(ICanalRepository canalRepository, ITabularConversion tabularconversion)
        {
            _canalRepository = canalRepository;
            _tabularconversion = tabularconversion;
        }
        public async Task<List<TabularApi>> GetCanal(ConsultaTabular tabularQuery)
        {
            DataSet resultado = new DataSet();
            DataTable result = await _canalRepository.GetCanal(tabularQuery);
            resultado.Tables.Add(result);

            return _tabularconversion.GenerarDetalle(resultado);
        }
    }
}
