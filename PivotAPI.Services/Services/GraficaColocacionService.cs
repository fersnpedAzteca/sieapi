﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Core.Filters;
using PivotAPI.Core.Entities;

namespace PivotAPI.Services.Services
{
    public class GraficaColocacionService : IGraficaColocacionService
    {
        private readonly IGraficaColocacionRepository _graficaColocacionRepository;
        private readonly ITabularConversion _tabularconversion;
        private readonly IGraficaConversion _graficaConversion;
        public GraficaColocacionService(IGraficaColocacionRepository graficaColocacionRepository, ITabularConversion tabularconversion, IGraficaConversion graficaConversion)
        {
            _graficaColocacionRepository = graficaColocacionRepository;
            _tabularconversion = tabularconversion;
            _graficaConversion = graficaConversion;
        }
        public async Task<DataTable> GetGraficaColocacion(ConsultaTabular tabularQuery)
        {
            DataTable result = await _graficaColocacionRepository.GetGraficaColocacion(tabularQuery);

            var resultadoPivoteado = _graficaConversion.Pivot(result, result.Columns["FiAnioSemana"], result.Columns["FnMonto"]);


            return resultadoPivoteado;
        }
    }
}
