﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Core.Filters;
using PivotAPI.Core.Entities;

namespace PivotAPI.Services.Services
{
    public class OriginacionService : IOriginacionService
    {
        private readonly IOriginacionRepository _originacionRepository;
        private readonly ITabularConversion _tabularconversion;
        public OriginacionService(IOriginacionRepository originacionRepository, ITabularConversion tabularconversion)
        {
            _originacionRepository = originacionRepository;
            _tabularconversion = tabularconversion;
        }
        public async Task<List<TabularApi>> GetOriginacion(ConsultaTabular tabularQuery)
        {
            DataSet resultado = new DataSet();
            DataTable result = await _originacionRepository.GetOriginacion(tabularQuery);
            resultado.Tables.Add(result);

            return _tabularconversion.GenerarDetalle(resultado);

            //return _tabularconversion.GenerarPorcentaje(result);
        }


    }
}
