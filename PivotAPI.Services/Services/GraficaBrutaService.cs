﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Core.Filters;
using PivotAPI.Core.Entities;

namespace PivotAPI.Services.Services
{
    public class GraficaBrutaService : IGraficaBrutaService
    {
        private readonly IGraficaBrutaRepository _graficaBrutaRepository;
        private readonly ITabularConversion _tabularconversion;
        private readonly IGraficaConversion _graficaConversion;
        public GraficaBrutaService(IGraficaBrutaRepository graficaBrutaRepository, ITabularConversion tabularconversion, IGraficaConversion graficaConversion)
        {
            _graficaBrutaRepository = graficaBrutaRepository;
            _tabularconversion = tabularconversion;
            _graficaConversion = graficaConversion;
        }
        public async Task<List<TabularApi>> GetGraficaBruta(ConsultaTabular tabularQuery)
        {
            DataSet resultado = new DataSet();
            DataTable result = await _graficaBrutaRepository.GetGraficaBruta(tabularQuery);
            resultado.Tables.Add(result);

            //var resultadoPivoteado = _graficaConversion.Pivot(result, result.Columns["FiAnioSemana"], result.Columns["FnMonto"]);

            return _graficaConversion.GenerarDetalle(resultado);
            //return resultadoPivoteado;
        }
    }
}
