﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Core.Filters;
using PivotAPI.Core.Entities;

namespace PivotAPI.Services.Services
{
    public class GeografiaService : IGeografiaService
    {
        private readonly IGeografiaRepository _geografiaRepository;
        private readonly ITabularConversion _tabularconversion;
        public GeografiaService(IGeografiaRepository geografiaRepository, ITabularConversion tabularconversion)
        {
            _geografiaRepository = geografiaRepository;
            _tabularconversion = tabularconversion;
        }
        public async Task<List<TabularApi>> GetGeografia(ConsultaTabular tabularQuery)
        {
            DataSet resultado = new DataSet();
            DataTable result = await _geografiaRepository.GetGeografia(tabularQuery);
            resultado.Tables.Add(result);

            return _tabularconversion.GenerarDetalle(resultado);

            //return _tabularconversion.GenerarPorcentaje(result);
        }
    }
}
