﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Core.Filters;
using PivotAPI.Core.Entities;

namespace PivotAPI.Services.Services
{
    public class FormatoService : IFormatoService
    {
        private readonly IFormatoRepository _formatoRepository;
        private readonly ITabularConversion _tabularconversion;
        public FormatoService(IFormatoRepository formatoRepository, ITabularConversion tabularconversion)
        {
            _formatoRepository = formatoRepository;
            _tabularconversion = tabularconversion;
        }
        public async Task<List<TabularApi>> GetFormato(ConsultaTabular tabularQuery)
        { 

            DataSet resultado = new DataSet();
            DataTable result = await _formatoRepository.GetFormato(tabularQuery);
            resultado.Tables.Add(result);

            return _tabularconversion.GenerarDetalle(resultado);

            //return _tabularconversion.GenerarPorcentaje(result);
        }

    }
}
