﻿using PivotAPI.Core.Entities;
using PivotAPI.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PivotAPI.Services.Utils
{
    public class GraficaConversion : IGraficaConversion
    {
        public GraficaConversion()
        {

        }
        public List<TabularApi> GenerarDetalle(DataSet data)
        {
            List<TabularApi> response = new List<TabularApi>();
            TabularApi rows = new TabularApi();
            rows.data = new List<Data>();
            rows.columna = new List<Columna>();


            return response;
        }
        public DataTable Pivot(DataTable data, DataColumn pivoteHeader, DataColumn ValorPivoteo)
        {

            DataTable temp = data.Copy();
            temp.Columns.Remove(pivoteHeader.ColumnName);
            temp.Columns.Remove(ValorPivoteo.ColumnName);
            string[] ListaColumnas = temp.Columns.Cast<DataColumn>()
                .Select(c => c.ColumnName).ToArray();

            DataTable tablaPivote = temp.DefaultView.ToTable(true, ListaColumnas).Copy();
            tablaPivote.PrimaryKey = tablaPivote.Columns.Cast<DataColumn>().ToArray();
            data.AsEnumerable()
                .Select(r => r[pivoteHeader.ColumnName].ToString())
                .Distinct().ToList()
                .ForEach(c => tablaPivote.Columns.Add(c, ValorPivoteo.DataType));

            foreach (DataRow row in data.Rows)
            {
                DataRow actualRow = tablaPivote.Rows.Find(
                    ListaColumnas
                        .Select(c => row[c])
                        .ToArray());
                actualRow[row[pivoteHeader.ColumnName].ToString()] = row[ValorPivoteo.ColumnName];
            }

            return tablaPivote;
        }
    }
}
