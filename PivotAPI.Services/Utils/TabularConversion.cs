﻿using PivotAPI.Core.Entities;
using PivotAPI.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PivotAPI.Services.Utils
{
    public class TabularConversion : ITabularConversion
    {
        public TabularConversion()
        {

        }

        public List<TabularApi> GenerarDetalle(DataSet data)
        {

            List<TabularApi> response = new List<TabularApi>();
            TabularApi rows = new TabularApi();
            rows.data = new List<Data>();
            rows.columna = new List<Columna>();


            ////Generar tabla para los vs
            //DataTable tabla = new DataTable()
            //{
            //    Columns = {
            //        { "VS_OBJETIVO", typeof(double) },
            //        { "VS_SEMANA_ANTERIOR", typeof(double)},
            //        { "VS_ANIO_ANTERIOR", typeof(double)} // typeof(string) is implied
            //    },
            //};

            //Nombre de las columnas para la tabla de la consulta
            foreach (DataColumn col in data.Tables[0].Columns)
            {
                Columna columna = new Columna();

                if (col.ColumnName != "Id")
                {
                    switch (col.ColumnName)
                    {
                        case "AnioAnt":
                            columna.campo = col.ColumnName;
                            columna.nombre_columna = "Año Anterior";
                            break;
                        case "SemAnt":
                            columna.campo = col.ColumnName;
                            columna.nombre_columna = "Semana Anterior";
                            break;
                        case "Objetivo":
                            columna.campo = col.ColumnName;
                            columna.nombre_columna = "Objetivo";
                            break;
                        case "Real":
                            columna.campo = col.ColumnName;
                            columna.nombre_columna = "Real";
                            break;
                        case "Descripcion":
                            columna.campo = col.ColumnName;
                            columna.nombre_columna = "Descripción";
                            break;
                        case "VsObjetivo":
                            columna.campo = col.ColumnName;
                            columna.nombre_columna = "Vs Objetivo";
                            break;
                        case "VsAnioAnt":
                            columna.campo = col.ColumnName;
                            columna.nombre_columna = "Vs Año Anterior";
                            break;
                        case "VsSemAnt":
                            columna.campo = col.ColumnName;
                            columna.nombre_columna = "Vs Semana Anterior";
                            break;
                    }
                    rows.columna.Add(columna);
                }


            }
            ////Nombre de las columnas para la tabla creada para los vs
            //foreach (DataColumn col in tabla.Columns)
            //{
            //    Columna columna = new Columna();

            //    switch (col.ColumnName)
            //    {
            //        case "VS_ANIO_ANTERIOR":
            //            columna.campo = col.ColumnName;
            //            columna.nombre_columna = "Vs Año Anterior";
            //            break;
            //        case "VS_SEMANA_ANTERIOR":
            //            columna.campo = col.ColumnName;
            //            columna.nombre_columna = "Vs Semana Anterior";
            //            break;
            //        case "VS_OBJETIVO":
            //            columna.campo = col.ColumnName;
            //            columna.nombre_columna = "Vs Objetivo";
            //            break;
            //    }
            //    rows.columna.Add(columna);

            //}

            //Variables aray
            double[] totalAnioAnt = new double[] { 1, 1 };
            double[] totalSemAnt = new double[] { 1, 1 };
            double[] totalObjetivo = new double[] { 1, 1 };
            double[] totalReal = new double[] { 1, 1 };

            double[] vstotalAnioAnt = new double[] { 1, 1 };
            double[] vstotalSemAnt = new double[] { 1, 1 };
            double[] vstotalObjetivo = new double[] { 1, 1 };

            //Recolectar datos de las 2 tablas (2 querys)

            for (int i = 0; i < data.Tables.Count; i++)
            {
                DataColumnCollection columns = data.Tables[i].Columns;
                //tabla.Clear();
                if (!(data.Tables[i].Rows.Count <= 0) )
                {

                    totalAnioAnt[i] = Convert.ToDouble(data.Tables[i].Compute("Sum(AnioAnt)", string.Empty));
                    totalSemAnt[i] = Convert.ToDouble(data.Tables[i].Compute("Sum(SemAnt)", string.Empty).ToString());
                    totalObjetivo[i] = Convert.ToDouble(data.Tables[i].Compute("Sum(Objetivo)", string.Empty).ToString());
                    totalReal[i] = Convert.ToDouble(data.Tables[i].Compute("Sum(Real)", string.Empty).ToString());
                    vstotalAnioAnt[i] = Convert.ToDouble(data.Tables[i].Compute("Sum(VsAnioAnt)", string.Empty).ToString());
                    vstotalSemAnt[i] = Convert.ToDouble(data.Tables[i].Compute("Sum(VsSemAnt)", string.Empty).ToString());
                    vstotalObjetivo[i] = Convert.ToDouble(data.Tables[i].Compute("Sum(VsObjetivo)", string.Empty).ToString());
                    // rows


                    foreach (DataRow row in data.Tables[i].Rows)
                    {
                        Data dt = new Data();
                        dt.anio_anterior = new List<Unidades>();
                        dt.semana_anterior = new List<Unidades>();
                        dt.objetivo = new List<Unidades>();
                        dt.mtoreal = new List<Unidades>();
                        dt.descripcion = new List<Descripcion>();
                        dt.vs_objetivo = new List<Unidades>();
                        dt.vs_anio_anterior = new List<Unidades>();
                        dt.vs_semana_anterior = new List<Unidades>();

                        dt.anio_anterior.Add(new Unidades()
                        {
                            unidades = (row["AnioAnt"]).ToString(),
                            porcentaje = (totalAnioAnt[i] != 0) ? (Convert.ToDouble(row["AnioAnt"]) * 100 / totalAnioAnt[i]).ToString() : "0"
                        });

                        dt.semana_anterior.Add(new Unidades()
                        {
                            unidades = (row["SemAnt"]).ToString(),
                            porcentaje = (totalSemAnt[i] != 0) ? (Convert.ToDouble(row["SemAnt"]) * 100 / totalSemAnt[i]).ToString() : "0"
                        });

                        dt.objetivo.Add(new Unidades()
                        {
                            unidades = (row["Objetivo"]).ToString(),
                            porcentaje = (totalObjetivo[i] != 0) ? (Convert.ToDouble(row["Objetivo"]) * 100 / totalObjetivo[i]).ToString() : "0"
                        });

                        dt.mtoreal.Add(new Unidades()
                        {
                            unidades = (row["Real"]).ToString(),
                            porcentaje = (totalReal[i] != 0) ? (Convert.ToDouble(row["Real"]) * 100 / totalReal[i]).ToString() : "0"
                        });

                        dt.descripcion.Add(new Descripcion()
                        {
                            descripcion = Convert.ToString(row["Descripcion"]),
                            valor = Convert.ToString(row["Id"])
                        });

                        dt.vs_anio_anterior.Add(new Unidades()
                        {
                            unidades = (row["VsAnioAnt"]).ToString(),
                            porcentaje = Convert.ToDouble(row["AnioAnt"]) != 0 ? ((Convert.ToDouble(row["Real"]) - Convert.ToDouble(row["AnioAnt"])) * 100 / Convert.ToDouble(row["AnioAnt"])).ToString() : "0"
                        });

                        dt.vs_semana_anterior.Add(new Unidades()
                        {
                            unidades = (row["VsSemAnt"]).ToString(),
                            porcentaje = Convert.ToDouble(row["SemAnt"]) != 0 ? ((Convert.ToDouble(row["Real"]) - Convert.ToDouble(row["SemAnt"])) * 100 / Convert.ToDouble(row["SemAnt"])).ToString() : "0"
                        });

                        dt.vs_objetivo.Add(new Unidades()
                        {
                            unidades = (row["VsObjetivo"]).ToString(),
                            porcentaje = Convert.ToDouble(row["Objetivo"]) != 0 ? ((Convert.ToDouble(row["Real"]) - Convert.ToDouble(row["Objetivo"])) * 100 / Convert.ToDouble(row["Objetivo"])).ToString() : "0"
                        });

                        // Creando la tabla Vs, para la sumatoria de totales
                        //tabla.Rows.Add(dt.vs_objetivo[0].unidades, dt.vs_semana_anterior[0].unidades, dt.vs_anio_anterior[0].unidades);

                        rows.data.Add(dt);

                    }

                    //TOTALES VS
                    //vstotalAnioAnt[i] = Convert.ToDouble(tabla.Compute("Sum(VS_ANIO_ANTERIOR)", string.Empty));
                    //vstotalSemAnt[i] = Convert.ToDouble(tabla.Compute("Sum(VS_SEMANA_ANTERIOR)", string.Empty).ToString());
                    //vstotalObjetivo[i] = Convert.ToDouble(tabla.Compute("Sum(VS_OBJETIVO)", string.Empty).ToString());


                    //TOTALES ULTIMO REGISTRO
                    Data dtt = new Data();
                    dtt.anio_anterior = new List<Unidades>();
                    dtt.semana_anterior = new List<Unidades>();
                    dtt.objetivo = new List<Unidades>();
                    dtt.mtoreal = new List<Unidades>();
                    dtt.descripcion = new List<Descripcion>();
                    dtt.vs_objetivo = new List<Unidades>();
                    dtt.vs_anio_anterior = new List<Unidades>();
                    dtt.vs_semana_anterior = new List<Unidades>();


                    dtt.descripcion.Add(new Descripcion()
                    {
                        descripcion = (i == 1) ? Convert.ToString("Total Venta de Mercancias") : Convert.ToString("Total de Venta Red Única"),
                        valor = Convert.ToString("TOTAL")
                    });

                    dtt.anio_anterior.Add(new Unidades()
                    {
                        unidades = (i == 1) ? (totalAnioAnt[i] + totalAnioAnt[0]).ToString() : (totalAnioAnt[i]).ToString(),
                        porcentaje = (totalAnioAnt[i] != 0) ? (Convert.ToDouble(totalAnioAnt[i]) * 100 / totalAnioAnt[i]).ToString() : "0"
                    });

                    dtt.semana_anterior.Add(new Unidades()
                    {
                        unidades = (i == 1) ? (totalSemAnt[i] + totalSemAnt[0]).ToString() : totalSemAnt[i].ToString(),
                        porcentaje = (totalSemAnt[i] != 0) ? (Convert.ToDouble(totalSemAnt[i]) * 100 / totalSemAnt[i]).ToString() : "0"
                    });

                    dtt.objetivo.Add(new Unidades()
                    {
                        unidades = (i == 1) ? (totalObjetivo[i] + totalObjetivo[0]).ToString() : (totalObjetivo[i]).ToString(),
                        porcentaje = (totalObjetivo[i] != 0) ? (Convert.ToDouble(totalObjetivo[i]) * 100 / totalObjetivo[i]).ToString() : "0"
                    });

                    dtt.mtoreal.Add(new Unidades()
                    {
                        unidades = (i == 1) ? (totalReal[i] + totalReal[0]).ToString() : (totalReal[i]).ToString(),
                        porcentaje = (totalReal[i] != 0) ? (Convert.ToDouble(totalReal[i]) * 100 / totalReal[i]).ToString() : "0"
                    });

                    dtt.vs_anio_anterior.Add(new Unidades()
                    {
                        unidades = (i == 1) ? (vstotalAnioAnt[i] + vstotalAnioAnt[0]).ToString() : (vstotalAnioAnt[i]).ToString(),
                        porcentaje = (i == 1) ? ((totalAnioAnt[i] != 0) ? (Convert.ToDouble(vstotalAnioAnt[i] + vstotalAnioAnt[0]) * 100 / (totalAnioAnt[i] + totalAnioAnt[0])).ToString() : "0") : ((totalAnioAnt[i] != 0) ? (Convert.ToDouble(vstotalAnioAnt[i]) * 100 / (totalAnioAnt[i] + totalAnioAnt[0])).ToString() : "0")

                    });

                    dtt.vs_semana_anterior.Add(new Unidades()
                    {
                        unidades = (i == 1) ? (vstotalSemAnt[i] + vstotalSemAnt[0]).ToString() : (vstotalSemAnt[i]).ToString(),
                        porcentaje = (i == 1) ? ((totalSemAnt[i] != 0) ? (Convert.ToDouble(vstotalSemAnt[i] + vstotalSemAnt[0]) * 100 / (totalSemAnt[i] + totalSemAnt[0])).ToString() : "0") : (totalSemAnt[i] != 0) ? (Convert.ToDouble(vstotalSemAnt[i]) * 100 / (totalSemAnt[i])).ToString() : "0"
                    });

                    dtt.vs_objetivo.Add(new Unidades()
                    {
                        unidades = (i == 1) ? (vstotalObjetivo[i] + vstotalObjetivo[0]).ToString() : (vstotalObjetivo[i]).ToString(),
                        porcentaje = (i == 1) ? ((totalObjetivo[i] != 0) ? (Convert.ToDouble(vstotalObjetivo[i] + vstotalObjetivo[0]) * 100 / (totalObjetivo[i] + totalObjetivo[0])).ToString() : "0") : (totalObjetivo[i] != 0) ? (Convert.ToDouble(vstotalObjetivo[i]) * 100 / (totalObjetivo[i])).ToString() : "0"
                    });

                    rows.data.Add(dtt);

                }

            }
       
            //Agregar % de participacion
            if (data.Tables.Count > 1)
            {
                //TOTALES ULTIMO REGISTRO
                Data dtt = new Data();
                dtt.anio_anterior = new List<Unidades>();
                dtt.semana_anterior = new List<Unidades>();
                dtt.objetivo = new List<Unidades>();
                dtt.mtoreal = new List<Unidades>();
                dtt.descripcion = new List<Descripcion>();
                dtt.vs_objetivo = new List<Unidades>();
                dtt.vs_anio_anterior = new List<Unidades>();
                dtt.vs_semana_anterior = new List<Unidades>();

                var anio_anterior = (totalAnioAnt[1] != 0) ? (Convert.ToDouble(totalAnioAnt[0]) * 100 / (totalAnioAnt[1] + totalAnioAnt[0])) : 0;
                var semana_anterior = (totalSemAnt[1] != 0) ? (Convert.ToDouble(totalSemAnt[0]) * 100 / (totalSemAnt[1] + totalSemAnt[0])) : 0;
                var objetivo = (totalObjetivo[1] != 0) ? (Convert.ToDouble(totalObjetivo[0]) * 100 / (totalObjetivo[1] + totalObjetivo[0])) : 0;
                var mtoreal = (totalReal[1] != 0) ? (Convert.ToDouble(totalReal[0]) * 100 / (totalReal[1] + totalReal[0])) : 0;

                dtt.descripcion.Add(new Descripcion()
                {
                    descripcion = "% Participación Red Única / Total de Venta Red Única",
                    valor = "TOTAL"
                });

                dtt.anio_anterior.Add(new Unidades()
                {
                    unidades = "0",
                    porcentaje = anio_anterior.ToString()
                }); ;

                dtt.semana_anterior.Add(new Unidades()
                {
                    unidades = "0",
                    porcentaje = semana_anterior.ToString()
                });

                dtt.objetivo.Add(new Unidades()
                {
                    unidades = "0",
                    porcentaje = objetivo.ToString()
                });

                dtt.mtoreal.Add(new Unidades()
                {
                    unidades = "0",
                    porcentaje = mtoreal.ToString()
                });

                dtt.vs_anio_anterior.Add(new Unidades()
                {
                    unidades = "0",
                    porcentaje = (mtoreal - anio_anterior).ToString()

                });

                dtt.vs_semana_anterior.Add(new Unidades()
                {
                    unidades = "0",
                    porcentaje = (mtoreal - semana_anterior).ToString()
                });

                dtt.vs_objetivo.Add(new Unidades()
                {
                    unidades = "0",
                    porcentaje = (mtoreal - objetivo).ToString()
                });

                rows.data.Add(dtt);
            }
            response.Add(rows);
            return response;
        }
        public List<TabularGeneral> GenerarPorcentaje(DataTable data)
        {
            List<TabularGeneral> response = new List<TabularGeneral>();
            DataColumnCollection columns = data.Columns;


            if (!(data.Rows.Count <= 0) && columns.Contains("AnioAnt") && columns.Contains("SemAnt") && columns.Contains("Real") && columns.Contains("vsSemAnt") && columns.Contains("vsAnioAnt") && columns.Contains("Id") && columns.Contains("Descripcion"))
            {
                var totalAnioAnt = Convert.ToDecimal(data.Compute("Sum(AnioAnt)", string.Empty).ToString());
                var totalSemAnt = Convert.ToDecimal(data.Compute("Sum(SemAnt)", string.Empty).ToString());
                var totalObjetivo = Convert.ToDecimal(data.Compute("Sum(Objetivo)", string.Empty).ToString());
                var totalReal = Convert.ToDecimal(data.Compute("Sum(Real)", string.Empty).ToString());
                var totalVsSemAnt = Convert.ToDecimal(data.Compute("Sum(vsSemAnt)", string.Empty).ToString());
                var totalVsAnioAnt = Convert.ToDecimal(data.Compute("Sum(vsAnioAnt)", string.Empty).ToString());
                foreach (DataRow row in data.Rows)
                {

                    TabularGeneral registro = new TabularGeneral()
                    {
                        AnioAnt = Convert.ToDecimal(row["AnioAnt"]),
                        AnioAntPorc = (totalAnioAnt != 0) ? Convert.ToInt32(Convert.ToDecimal(row["AnioAnt"]) * 100 / totalAnioAnt) : 0,
                        SemAnt = Convert.ToDecimal(row["SemAnt"]),
                        SemAntPorc = (totalSemAnt != 0) ? Convert.ToInt32(Convert.ToDecimal(row["SemAnt"]) * 100 / totalSemAnt) : 0,
                        Objetivo = Convert.ToDecimal(row["Objetivo"]),
                        ObjetivoPorc = (totalObjetivo != 0) ? Convert.ToInt32(Convert.ToDecimal(row["Objetivo"]) * 100 / totalObjetivo) : 0,
                        Real = Convert.ToDecimal(row["Real"]),
                        RealPorc = (totalReal != 0) ? Convert.ToInt32(Convert.ToDecimal(row["Real"]) * 100 / totalReal) : 0,
                        idTabular = Convert.ToInt32(row["Id"]),
                        Descripcion = row["Descripcion"].ToString(),
                        VsObjetivo = Convert.ToDecimal(row["vsObjetivo"]),
                        VsObjetivoPorc = (totalObjetivo != 0) ? Convert.ToInt32(Convert.ToDecimal(row["Objetivo"]) * 100 / totalObjetivo) : 0,
                        VsSemAnt = Convert.ToDecimal(row["vsSemAnt"]),
                        VsSemAntPorc = (totalVsSemAnt != 0) ? Convert.ToInt32(Convert.ToDecimal(row["vsSemAnt"]) * 100 / totalVsSemAnt) : 0,
                        VsAnioAnt = Convert.ToDecimal(row["vsAnioAnt"]),
                        VsAnioAntPorc = (totalVsAnioAnt != 0) ? Convert.ToInt32(Convert.ToDecimal(row["vsAnioAnt"]) * 100 / totalVsAnioAnt) : 0,
                    };

                    response.Add(registro);
                }

                TabularGeneral registroTotal = new TabularGeneral()
                {
                    AnioAnt = totalAnioAnt,
                    AnioAntPorc = 100,
                    SemAnt = totalSemAnt,
                    SemAntPorc = 100,
                    Objetivo = totalObjetivo,
                    ObjetivoPorc = 100,
                    Real = totalReal,
                    RealPorc = 100,
                    idTabular = 0,
                    Descripcion = "Total",
                    VsObjetivo = totalObjetivo,
                    VsObjetivoPorc = 100,
                    VsSemAnt = totalVsSemAnt,
                    VsSemAntPorc = 100,
                    VsAnioAnt = totalVsAnioAnt,
                    VsAnioAntPorc = 100,
                };

                response.Add(registroTotal);
            }
            return response;
        }

        public DataTable GenerarTabularDeDataSet(DataSet data)
        {
            DataTable tableResult = new DataTable();
            tableResult.Columns.Add("AnioAnt", typeof(decimal));
            tableResult.Columns.Add("SemAnt", typeof(decimal));
            tableResult.Columns.Add("Real", typeof(decimal));
            tableResult.Columns.Add("Id", typeof(int));
            tableResult.Columns.Add("Descripcion", typeof(string));
            tableResult.Columns.Add("vsSemAnt", typeof(decimal));
            tableResult.Columns.Add("vsAnioAnt", typeof(decimal));


            if (data.Tables.Contains("SemAnt") && data.Tables.Contains("AnioAnt") && data.Tables.Contains("Real"))
            {
                if (data.Tables["SemAnt"].Columns.Contains("Capital") && data.Tables["AnioAnt"].Columns.Contains("Capital") && data.Tables["Real"].Columns.Contains("Capital") && data.Tables["Real"].Columns.Contains("Id") && data.Tables["Real"].Columns.Contains("Descripcion"))
                {
                    if (!(data.Tables[0].Rows.Count <= 0 && data.Tables[1].Rows.Count <= 0 && data.Tables[2].Rows.Count <= 0))
                    {
                        for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                        {
                            tableResult.Rows.Add();
                            tableResult.Rows[i]["SemAnt"] = data.Tables["SemAnt"].Rows[i]["Capital"];
                            tableResult.Rows[i]["AnioAnt"] = data.Tables["AnioAnt"].Rows[i]["Capital"];
                            tableResult.Rows[i]["Real"] = data.Tables["Real"].Rows[i]["Capital"];
                            tableResult.Rows[i]["Id"] = data.Tables["Real"].Rows[i]["Id"];
                            tableResult.Rows[i]["Descripcion"] = data.Tables["Real"].Rows[i]["Descripcion"];


                            tableResult.Rows[i]["vsSemAnt"] = Convert.ToDecimal(data.Tables["Real"].Rows[i]["Capital"].ToString()) - Convert.ToDecimal(data.Tables["SemAnt"].Rows[i]["Capital"].ToString());
                            tableResult.Rows[i]["vsAnioAnt"] = Convert.ToDecimal(data.Tables["Real"].Rows[i]["Capital"].ToString()) - Convert.ToDecimal(data.Tables["AnioAnt"].Rows[i]["Capital"].ToString());
                        }

                    }

                }
            }
            return tableResult;
        }
    }
}
