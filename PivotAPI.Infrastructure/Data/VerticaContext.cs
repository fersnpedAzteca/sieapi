﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vertica.Data.VerticaClient;

namespace PivotAPI.Infrastructure.Data
{
    public class VerticaContext : DbContext
    {
        private VerticaConnection _conn;
        private VerticaConnectionStringBuilder _builder;


        //private readonly string _connectionString;

        public VerticaContext()
        {

        }

        public VerticaContext(DbContextOptions<VerticaContext> options, IConfiguration configuration) : base(options)
        {
            // Configure connection properties
            VerticaConnectionStringBuilder builder = new VerticaConnectionStringBuilder();
            builder.Host = configuration["Vertica:Host"];
            builder.Database = configuration["Vertica:Database"];
            builder.User = configuration["Vertica:User"];
            builder.Password = configuration["Vertica:Password"];

            _builder = builder;
        }

        public async Task<DataTable> Leer(string queryString, List<VerticaParameter> parametros = null)
        {
            StringBuilder errorMessages = new StringBuilder();
            DataTable tabla = new DataTable();
            //try { 
            Conn();

            VerticaCommand command = _conn.CreateCommand();
            command.CommandText = queryString;
            if (parametros != null)
            {
                foreach (VerticaParameter parametro in parametros)
                {
                    command.Parameters.Add(parametro);
                }
            }
            VerticaDataReader dr = command.ExecuteReader();

            tabla.Load(dr);
            CloseCon();
            return tabla;


        }

        private void Conn()
        {
            _conn = new VerticaConnection(_builder.ToString());
            _conn.Open();
        }
        private void CloseCon()
        {
            _conn.Close();
            _conn = null;
            GC.Collect();
        }

    }
}


