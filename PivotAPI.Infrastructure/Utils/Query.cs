﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using PivotAPI.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PivotAPI.Infrastructure.Utils
{
    public class Query : IQuery
    {
        private readonly string _pathString;
        private readonly string _basePathString;

        public Query(IConfiguration configuration)
        {
            _pathString = configuration.GetSection("Scripts:path").Value;
            _basePathString = Directory.GetCurrentDirectory();

        }
        public string GetValue(string prop)
        {

            var myJsonString = File.ReadAllText(Path.Combine(_basePathString, _pathString));
            var myJObject = JObject.Parse(myJsonString);

            return myJObject.SelectToken(prop).ToString();
        }
        public string AddParams(string query, string parameterName, string parameterValue)
        {

            return query.Replace(parameterName, parameterValue);
        }
    }
}
