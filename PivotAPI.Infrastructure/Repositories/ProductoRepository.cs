﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Infrastructure.Data;
using Vertica.Data.VerticaClient;
using System.Globalization;
using PivotAPI.Core.Filters;

namespace PivotAPI.Infrastructure.Repositories
{
    public class ProductoRepository : IProductoRepository
    {
        private readonly VerticaContext _verticaContext;
        private readonly string _connectionString;
        private IQuery _query;

        public ProductoRepository(VerticaContext verticaContext, IConfiguration configuration, IQuery query)
        {
            _verticaContext = verticaContext;
            _query = query;


        }

        public async Task<DataTable> GetProducto(ConsultaTabular tabularQuery)
        {
            string query;
            string filtroTipoCliente = "";
            string filtroOriginacion = "";
            string filtroOriginacionJoin = "";
            string filtroOriginacionJoinPlan = "";
            string filtroGeografia = "";
            string filtroGeografiaJoin = "";
            string filtroFormato = "";
            string filtroFormatoJoin = "";
            string filtroFormatoJoinPlan = "";
            string filtroDia = "";
            string filtroProducto = "";

            List<VerticaParameter> paramsFechas = new List<VerticaParameter>();
            VerticaParameter paramFechas = new VerticaParameter("fecha", VerticaType.VarChar, tabularQuery.fecha);
            paramsFechas.Add(paramFechas);

            //Se consultan las fechas para el query en la tabla DIMPERIDOS
            DataTable fechas = await _verticaContext.Leer(_query.GetValue("General.FechasBtw"), paramsFechas);

            string semanaAntInf = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][0]);
            string semanaAntSup = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][1]);
            string anioAntInf = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][2]);
            string anioAntSup = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][3]);
            string fechaInf = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][4]);

            List<VerticaParameter> parametros = new List<VerticaParameter>();

            //Se agregan los paramtros dinamicamente al query de BD
            if (tabularQuery.producto != null)
            {
                filtroProducto += " AND p.id_Producto_Sup = " + tabularQuery.producto.ToString() + " ";
                query = _query.GetValue("Producto.TabularDrill");
            }
            else
            {
                query = _query.GetValue("Producto.TabularInicial");
            }
            if (!(tabularQuery.tipoCliente == null || tabularQuery.tipoCliente == -2))
            {
                filtroTipoCliente += " AND C.CLASIFICACION_CTE IN ( " + tabularQuery.tipoCliente.ToString() + " ) ";
            }

            if (tabularQuery.dia != null)
            {
                filtroDia += " AND c.dia_Sem = " + tabularQuery.dia.ToString() + " ";
            }
            if (tabularQuery.geografia != null)
            {
                filtroGeografia += " AND DG.ID_CC_PAPA = " + tabularQuery.geografia.ToString() + " ";
                filtroGeografiaJoin += " INNER JOIN schvtcd_raw_baz_colocacion.raw_cat_DrillGeografia DG ON C.ID_CC = DG.ID_CC ";
                //filtroGeografiaJoinPlan += " INNER JOIN schvtcd_raw_baz_colocacion.raw_cat_DrillGeografia DG ON DC.SUCURSAL = DG.ID_CENTRO ";
            }

            if (tabularQuery.originacion != null)
            {
                filtroOriginacion += " AND o.ID_ORIGEN_PADRE = " + tabularQuery.originacion.ToString() + " ";
                filtroOriginacionJoin += " INNER JOIN schvtcd_raw_baz_colocacion.raw_cat_DrillOriginacion o ON c.ID_ORIGEN = o.ID_ORIGEN ";
                filtroOriginacionJoinPlan += " INNER JOIN schvtcd_raw_baz_colocacion.raw_cat_DrillOriginacion o ON c.ID_ORIGEN_CUC = o.ID_ORIGEN ";
            }
            if (tabularQuery.formato != null)
            {
                filtroFormato += " AND f.ELEMENTO_PADRE = " + tabularQuery.formato.ToString() + " ";
                filtroFormatoJoin += " INNER JOIN schvtcd_raw_baz_colocacion.raw_cat_DrillFormato f ON c.ID_ELEMENTO = f.Elemento ";
                filtroFormatoJoinPlan += " INNER JOIN schvtcd_raw_baz_colocacion.raw_cat_DrillFormato f ON c.ELEMENTO = f.Elemento ";
            }

            VerticaParameter paramSemanaAntInf = new VerticaParameter("semanaAntInf", VerticaType.VarChar, semanaAntInf);
            VerticaParameter paramSemanaAntSup = new VerticaParameter("semanaAntSup", VerticaType.VarChar, semanaAntSup);
            VerticaParameter paramAnioAntInf = new VerticaParameter("anioAntInf", VerticaType.VarChar, anioAntInf);
            VerticaParameter paramAnioAntSup = new VerticaParameter("anioAntSup", VerticaType.VarChar, anioAntSup);
            VerticaParameter paramFechaInf = new VerticaParameter("fechaInf", VerticaType.VarChar, fechaInf);
            VerticaParameter paramFecha = new VerticaParameter("fecha", VerticaType.VarChar, tabularQuery.fecha);

            query = _query.AddParams(query, "@filtroTipoCliente", filtroTipoCliente);
            query = _query.AddParams(query, "@filtroOriginacionJoinPlan", filtroOriginacionJoinPlan);
            query = _query.AddParams(query, "@filtroOriginacionJoin", filtroOriginacionJoin);
            query = _query.AddParams(query, "@filtroOriginacion", filtroOriginacion);
            query = _query.AddParams(query, "@filtroFormatoJoinPlan", filtroFormatoJoinPlan);
            query = _query.AddParams(query, "@filtroFormatoJoin", filtroFormatoJoin);
            query = _query.AddParams(query, "@filtroFormato", filtroFormato);
            query = _query.AddParams(query, "@filtroDia", filtroDia);
            query = _query.AddParams(query, "@filtroProducto", filtroProducto);
            query = _query.AddParams(query, "@filtroGeografiaJoin", filtroGeografiaJoin);
            query = _query.AddParams(query, "@filtroGeografia", filtroGeografia);

            parametros.Add(paramFechaInf);
            parametros.Add(paramFecha);
            parametros.Add(paramSemanaAntInf);
            parametros.Add(paramSemanaAntSup);
            parametros.Add(paramAnioAntInf);
            parametros.Add(paramAnioAntSup);

            var dataPivot = await _verticaContext.Leer(query, parametros);

            return dataPivot;
        }
    }
}
