﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Infrastructure.Data;
using Vertica.Data.VerticaClient;
using System.Globalization;
using PivotAPI.Core.Filters;


namespace PivotAPI.Infrastructure.Repositories
{
    public class GraficaBrutaRepository : IGraficaBrutaRepository
    {
        private readonly VerticaContext _verticaContext;
        private readonly string _connectionString;
        private IQuery _query;

        public GraficaBrutaRepository(VerticaContext verticaContext, IConfiguration configuration, IQuery query)
        {
            _verticaContext = verticaContext;
            _query = query;


        }

        public async Task<DataTable> GetGraficaBruta(ConsultaTabular tabularQuery)
        {

            string query;
            string filtroTipoCliente = "";
            string filtroOriginacion = "";
            string filtroOriginacionPlan = "";

            List<VerticaParameter> paramsFechas = new List<VerticaParameter>();
            VerticaParameter paramFechas = new VerticaParameter("fecha", VerticaType.VarChar, tabularQuery.fecha);
            paramsFechas.Add(paramFechas);

            DataTable fechas = await _verticaContext.Leer(_query.GetValue("General.FechasGrafica"), paramsFechas);

            string anioAntepas = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][0]);
            string anioAnt = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][1]);

            List<VerticaParameter> parametros = new List<VerticaParameter>();
            query = _query.GetValue("ColocacionBruta.Grafica");

            //Se agregan los paramtros dinamicamente al query de BD
            if (!(tabularQuery.tipoCliente == null || tabularQuery.tipoCliente == -2))
            {
                filtroTipoCliente += " AND DC.FICLASIFICACIONCTE = " + tabularQuery.tipoCliente.ToString() + " ";
            }
            else
            {
                filtroTipoCliente += " AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ";
            }
            if (tabularQuery.originacion != null)
            {
                filtroOriginacion += " AND DC.FISUPERIOR = " + tabularQuery.originacion.ToString() + " ";
                filtroOriginacionPlan += " AND DC.FISUPERIOR = " + tabularQuery.originacion.ToString() + " ";
            }
            else
            {
                filtroOriginacion += " AND DC.FISUPERIOR = DC.FIORIGEN ";
                filtroOriginacionPlan += " AND DC.FISUPERIOR = DC.FiIdOrigenCUC ";
            }

            VerticaParameter paramAnioAntInf = new VerticaParameter("anioAntepas", VerticaType.VarChar, anioAntepas);
            VerticaParameter paramAnioAntSup = new VerticaParameter("anioAnt", VerticaType.VarChar, anioAnt);
            VerticaParameter paramFecha = new VerticaParameter("fecha", VerticaType.VarChar, tabularQuery.fecha);

            query = _query.AddParams(query, "@filtroTipoCliente", filtroTipoCliente);
            query = _query.AddParams(query, "@filtroOriginacionPlan", filtroOriginacionPlan);
            query = _query.AddParams(query, "@filtroOriginacion", filtroOriginacion);

            //parametros.Add(paramFechaInf);
            parametros.Add(paramFecha);

            parametros.Add(paramAnioAntInf);
            parametros.Add(paramAnioAntSup);

            var dataPivot = await _verticaContext.Leer(query, parametros);

            return dataPivot;
        }
    }
}
