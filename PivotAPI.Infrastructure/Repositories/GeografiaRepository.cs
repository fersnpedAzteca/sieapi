﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Infrastructure.Data;
using Vertica.Data.VerticaClient;
using System.Globalization;
using PivotAPI.Core.Filters;

namespace PivotAPI.Infrastructure.Repositories
{
    public class GeografiaRepository : IGeografiaRepository
    {
        private readonly VerticaContext _verticaContext;
        private readonly string _connectionString;
        private IQuery _query;

        public GeografiaRepository(VerticaContext verticaContext, IConfiguration configuration, IQuery query)
        {
            _verticaContext = verticaContext;
            _query = query;


        }

        public async Task<DataTable> GetGeografia(ConsultaTabular tabularQuery)
        {

            string query;
            string filtroTipoCliente = "";
            string filtroOriginacion = "";
            string filtroOriginacionJoin = "";
            string filtroOriginacionJoinPlan = "";
            string filtroProducto = "";
            string filtroProductoJoin = "";
            string filtroFormato = "";
            string filtroFormatoJoin = "";
            string filtroDia = "";
            string filtroGeografia = "";

            List<VerticaParameter> paramsFechas = new List<VerticaParameter>();
            VerticaParameter paramFechas = new VerticaParameter("fecha", VerticaType.VarChar, tabularQuery.fecha);
            paramsFechas.Add(paramFechas);

            DataTable fechas = await _verticaContext.Leer(_query.GetValue("General.FechasBtw"), paramsFechas);

            string semanaAntInf = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][0]);
            string semanaAntSup = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][1]);
            string anioAntInf = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][2]);
            string anioAntSup = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][3]);
            string fechaInf = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][4]);

            //VerticaParameter paramGeografia;
            List<VerticaParameter> parametros = new List<VerticaParameter>();
            query = _query.GetValue("Geografia.Tabular");

            if (!(tabularQuery.geografia == null))
            {
                filtroGeografia = " AND DG.ID_CC_PAPA = " + tabularQuery.geografia + " ";

            }
            else
            {
                filtroGeografia = " AND DG.ID_CC_PAPA = " + 6867 + " ";

            }
            if (!(tabularQuery.tipoCliente == null || tabularQuery.tipoCliente == -2))
            {
                filtroTipoCliente += " AND DC.CLASIFICACION_CTE IN ( " + tabularQuery.tipoCliente.ToString() + " ) ";
            }

            if (tabularQuery.originacion != null)
            {
                filtroOriginacion += " AND DO.ID_ORIGEN_PADRE = " + tabularQuery.originacion.ToString() + " ";
                filtroOriginacionJoin += " INNER JOIN schvtcd_raw_baz_colocacion.raw_cat_DrillOriginacion DO ON DC.ORIGEN = DO.ID_ORIGEN ";
                filtroOriginacionJoinPlan += " INNER JOIN schvtcd_raw_baz_colocacion.raw_cat_DrillOriginacion DO ON DC.ID_ORIGEN_CUC = DO.ID_ORIGEN ";
            }

            if (tabularQuery.formato != null)
            {
                filtroFormatoJoin += " INNER JOIN schvtcd_raw_baz_colocacion.raw_cat_DrillFormato DF ON DC.ELEMENTO = DF.ELEMENTO ";
                filtroFormato += " AND DF.ELEMENTO_PADRE = " + tabularQuery.formato.ToString() + " ";
            }

            if (tabularQuery.dia != null)
            {
                filtroDia += " AND DC.DIA_SEM = " + tabularQuery.dia.ToString() + " ";
            }

            if (tabularQuery.producto != null)
            {
                filtroProducto += " AND p.id_Producto_Sup = " + tabularQuery.producto.ToString() + " ";
                filtroProductoJoin += " INNER JOIN schvtcd_raw_baz_colocacion.raw_cat_DrillProductos p ON DC.ID_PRODUCTO = p.id_Producto ";
            }


            VerticaParameter paramSemanaAntInf = new VerticaParameter("semanaAntInf", VerticaType.VarChar, semanaAntInf);
            VerticaParameter paramSemanaAntSup = new VerticaParameter("semanaAntSup", VerticaType.VarChar, semanaAntSup);
            VerticaParameter paramAnioAntInf = new VerticaParameter("anioAntInf", VerticaType.VarChar, anioAntInf);
            VerticaParameter paramAnioAntSup = new VerticaParameter("anioAntSup", VerticaType.VarChar, anioAntSup);
            VerticaParameter paramFechaInf = new VerticaParameter("fechaInf", VerticaType.VarChar, fechaInf);
            VerticaParameter paramFecha = new VerticaParameter("fecha", VerticaType.VarChar, tabularQuery.fecha);

            query = _query.AddParams(query, "@filtroTipoCliente", filtroTipoCliente);
            query = _query.AddParams(query, "@filtroOriginacionJoinPlan", filtroOriginacionJoinPlan);
            query = _query.AddParams(query, "@filtroOriginacionJoin", filtroOriginacionJoin);
            query = _query.AddParams(query, "@filtroOriginacion", filtroOriginacion);
            query = _query.AddParams(query, "@filtroGeografia", filtroGeografia);
            query = _query.AddParams(query, "@filtroFormatoJoin", filtroFormatoJoin);
            query = _query.AddParams(query, "@filtroFormato", filtroFormato);
            query = _query.AddParams(query, "@filtroProductoJoin", filtroProductoJoin);
            query = _query.AddParams(query, "@filtroProducto", filtroProducto);
            query = _query.AddParams(query, "@filtroDia", filtroDia); 

            parametros.Add(paramFechaInf);
            parametros.Add(paramFecha);
            parametros.Add(paramSemanaAntInf);
            parametros.Add(paramSemanaAntSup);
            parametros.Add(paramAnioAntInf);
            parametros.Add(paramAnioAntSup);

            var dataPivot = await _verticaContext.Leer(query, parametros);

            return dataPivot;
        }
    }
}
