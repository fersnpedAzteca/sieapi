﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using PivotAPI.Core.Interfaces;
using PivotAPI.Infrastructure.Data;
using Vertica.Data.VerticaClient;
using System.Globalization;
using PivotAPI.Core.Filters;

namespace PivotAPI.Infrastructure.Repositories
{
    public class CanalRepository : ICanalRepository
    {
        private readonly VerticaContext _verticaContext;
        private readonly string _connectionString;
        private IQuery _query;

        public CanalRepository(VerticaContext verticaContext, IConfiguration configuration, IQuery query)
        {
            _verticaContext = verticaContext;
            _query = query;


        }

        public async Task<DataTable> GetCanal(ConsultaTabular tabularQuery)
        {

            string query;
            string filtroTipoCliente = "";
            string filtroOriginacion = "";
            string filtroOriginacionPlan = "";
            string filtroFormato = "";
            string filtroDia = "";


            List<VerticaParameter> paramsFechas = new List<VerticaParameter>();
            VerticaParameter paramFechas = new VerticaParameter("fecha", VerticaType.VarChar, tabularQuery.fecha);
            paramsFechas.Add(paramFechas);

            DataTable fechas = await _verticaContext.Leer(_query.GetValue("General.FechasBtw"), paramsFechas);

            string semanaAntInf = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][0]);
            string semanaAntSup = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][1]);
            string anioAntInf = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][2]);
            string anioAntSup = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][3]);
            string fechaInf = String.Format("{0:yyyy-MM-dd}", fechas.Rows[0][4]);

            List<VerticaParameter> parametros = new List<VerticaParameter>();
            query = _query.GetValue("Canal.Tabular");

            if (tabularQuery.originacion != null)
            {
                filtroOriginacion += " AND FISUPERIOR = " + tabularQuery.originacion.ToString() + " ";
            }/*
            else
            {
                filtroOriginacion += " AND FISUPERIOR = FIORIGEN ";
                filtroOriginacionPlan += " AND FISUPERIOR = FiIdOrigenCUC ";
            }*/
            if (!(tabularQuery.tipoCliente == null || tabularQuery.tipoCliente == -2))
            {
                filtroTipoCliente += " AND DC.FICLASIFICACIONCTE IN ( " + tabularQuery.tipoCliente.ToString() + " ) ";
                //filtroTipoCliente += " AND FIIDCLASIFICACIONCTE = " + tabularQuery.tipoCliente.ToString() + " ";
            }
            if (tabularQuery.dia != null)
            {
                filtroDia += " AND DC.FIDIASEM = " + tabularQuery.dia.ToString() + " ";
            }
            if (tabularQuery.formato != null)
            {
                filtroDia += " AND FIELEMENTO_PADRE = " + tabularQuery.formato.ToString() + " ";
            }

            VerticaParameter paramSemanaAntInf = new VerticaParameter("semanaAntInf", VerticaType.VarChar, semanaAntInf);
            VerticaParameter paramSemanaAntSup = new VerticaParameter("semanaAntSup", VerticaType.VarChar, semanaAntSup);
            VerticaParameter paramAnioAntInf = new VerticaParameter("anioAntInf", VerticaType.VarChar, anioAntInf);
            VerticaParameter paramAnioAntSup = new VerticaParameter("anioAntSup", VerticaType.VarChar, anioAntSup);
            VerticaParameter paramFechaInf = new VerticaParameter("fechaInf", VerticaType.VarChar, fechaInf);
            VerticaParameter paramFecha = new VerticaParameter("fecha", VerticaType.VarChar, tabularQuery.fecha);

            query = _query.AddParams(query, "@filtroTipoCliente", filtroTipoCliente);
            query = _query.AddParams(query, "@filtroOriginacionPlan", filtroOriginacionPlan);
            query = _query.AddParams(query, "@filtroOriginacion", filtroOriginacion);
            query = _query.AddParams(query, "@filtroFormato", filtroFormato);
            query = _query.AddParams(query, "@filtroDia", filtroDia);


            parametros.Add(paramFechaInf);
            parametros.Add(paramFecha);
            parametros.Add(paramSemanaAntInf);
            parametros.Add(paramSemanaAntSup);
            parametros.Add(paramAnioAntInf);
            parametros.Add(paramAnioAntSup);

            var dataPivot = await _verticaContext.Leer(query, parametros);

            return dataPivot;
        }
    }
}
